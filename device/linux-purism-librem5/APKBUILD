# Reference: <https://postmarketos.org/vendorkernel>
# Kernel config based on: arch/arm64/configs/librem5-devkit_defconfig
pkgname="linux-purism-librem5"
pkgver=5.3
pkgrel=2
pkgdesc="Purism Librem 5 phone kernel fork"
arch="aarch64"
_carch="arm64"
_flavor="purism-librem5"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev
	     devicepkg-dev bison flex openssl-dev"

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Source
_repository="linux-next"
# imx8-current-librem5 branch:
_commit="1309cc0a813e70af9b3d8c29d513e78715475de5"
# kconfig generated with: ARCH=arm64 make defconfig KBUILD_DEFCONFIG=librem5_defconfig
_config="config-$_flavor.$arch"
source="
	$pkgname-$_commit.tar.gz::https://source.puri.sm/Librem5/$_repository/-/archive/$_commit.tar.gz
        0001-force-host-mode-for-usb.patch
	$_config
"
builddir="$srcdir/$_repository-$_commit"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		downstreamkernel_prepare "$srcdir" "$builddir" "$_config" "$_carch" "$HOSTCC"
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"

}
sha512sums="85cf8671c4d9481477edbb994423b3745cac684e7a056db889a97a18eb8e26778a900b7151865e8bc79f4394f0bb86c360ecf9eee01cbed6e7d4cbe2862b9e0c  linux-purism-librem5-1309cc0a813e70af9b3d8c29d513e78715475de5.tar.gz
914a28ab4117ac1b349c6daae3538158f2346745e2f85f03667601eb6410db91f0f9805011f14ff2168da9c62821c921737ddfaf3d9babffbb2d9b84a6e83ac1  0001-force-host-mode-for-usb.patch
79046403a26384518ccdb849abc9cfdfe6873986286e4ce97af3e8f633fdb6c8f55b18c954b36edffed3f42f08cbc68f3311e1157f573ee8759fa3da800ad91e  config-purism-librem5.aarch64"
